const router = require('express').Router();
const AuthCtrl = require('../controller/auth');
const passport = require('../middleware/passport-setup');

router.get('/login', AuthCtrl.login);
router.get('/logout', AuthCtrl.logout);
/** As i understand, this is the first request (in my world this will be handled by vue), this returns with
 * a token, which will be exchanged for the profile in the redirect url (which will be called by vue
 * to the /google/redirect endpoint)
 * The endpoint is called from the redirect uri with a query parameter /auth/google/redirect?code=4/9wDMkyD6Vyzrwwmw...
 * Which will fire the callback function in the passport configuration 
 */
router.get('/google', passport.authenticate('google', {
  /** Options to authenticate, scope determines what information is required */
  scope: ['profile', 'email'],
  session: false
}));

router.post('/google/redirect', passport.authenticate('google', {
  scope: ['profile', 'email'],
  session: false
}), AuthCtrl.google);


module.exports = router;