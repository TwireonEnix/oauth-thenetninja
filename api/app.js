require('dotenv').config();
const express = require('express');
const app = express();

require('./helpers/db')();
require('./helpers/pipeline')(app);

module.exports = app;