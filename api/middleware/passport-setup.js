const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');

/** Redirect URI:
 * where do we want to redirect the user when the user clicks allow.
 * In this case we will redirect them to /auth/google
 * This is within the configuration of the google api credential itself
 * Then we will add this configuration to the google strategy object
 * as a callbackURL property
 */

passport.use('google', new GoogleStrategy({
  /** Options for the google strat Used google api behind the scenes, it needs
   * a api id and a secret. Both extracted from google apis
   */
  callbackURL: '/auth/google/redirect',
  clientID: process.env.GOOGLE_CLIENTID,
  clientSecret: process.env.GOOGLE_SECRET
}, (accessToken, refreshToken, profile, done) => {
  console.log(accessToken);

  /** Done */
  done(null, profile);
}));

/** I'm still not quite sure what serialize and deserialize do in my apps
 * i think it has something to do with sessions, but as i only need the 
 * profile info to login the user with their info and let the controller
 * handle the rest of the authentication.
 * Serialize user requires a passport initialization in the req pipeline.
 */
passport.serializeUser((user, done) => done(null, user));

module.exports = passport;