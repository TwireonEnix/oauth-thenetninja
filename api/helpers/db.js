const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.exports = () => {
  const db = 'oauthTutorial'
  const connectionString =
    `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_PASS}@${process.env.ATLAS_HOST}/${db}?retryWrites=true01`;
  mongoose.connect(connectionString, { useNewUrlParser: true })
    .then(() => { console.log('Connected to atlas') })
    .catch(err => { throw new Error(err); });
}