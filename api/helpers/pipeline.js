const express = require('express');
const auth = require('../routes/auth');
const passport = require('passport');
const cors = require('../middleware/cors');

module.exports = app => {
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(cors);
  app.use(passport.initialize());
  app.use(require('morgan')('dev'));
  app.set('view engine', 'ejs');
  app.get('/', (req, res) => res.render('home'));
  app.use('/auth', auth);
}