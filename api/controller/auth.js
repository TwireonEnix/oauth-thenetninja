module.exports = {

  login(req, res) {
    res.render('login');
  },

  logout(req, res) {
    res.send('Logging out');
  },

  google(req, res) {
    console.log(JSON.stringify(req.user, null, 2));
    res.send(req.user);
  }

}