const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  name: String,
  familyName: String,
  email: String,
  googleId: String,
});

module.exports = mongoose.model('user', userSchema);