// import axios from 'axios';
import router from '../router';


export default {
  state: {
    token: null
  },
  getters: {
    isLoggedIn: state => !!state.token
  },
  mutations: {
    setToken: (state, token) => state.token = token
  },
  actions: {
    login: context => context.commit('setToken', 'abc123'),
    logout: context => {
      context.commit('setToken', null);
      router.push('/');
    }
  }
};