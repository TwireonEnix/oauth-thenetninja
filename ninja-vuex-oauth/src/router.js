import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home';
import LoggedIn from './views/LoggedIn';
import store from './store';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/',
    name: 'home',
    component: Home
  }, {
    path: '/logged',
    name: 'logged',
    component: LoggedIn,
    beforeEnter(to, from, next) {
      store.getters.isLoggedIn ? next() : next('/denied');
    }
  }, {
    path: '/denied',
    name: 'denied',
    component: () => import('./views/NoAccess')
  }]
});