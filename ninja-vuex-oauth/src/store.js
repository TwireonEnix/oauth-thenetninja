import Vue from 'vue';
import Vuex from 'vuex';
import auth from './store/auth.store';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [
      { name: 'Banana Skin', price: 20 },
      { name: 'Shiny Star', price: 40 },
      { name: 'Green Shells', price: 60 },
      { name: 'Red Shells', price: 80 }
    ]
  },
  getters: {
    getProducts: state => state.products
  },
  mutations: {
    deleteOne: context => context.products.pop()
  },
  actions: {
    triggerDeleteOne: context => context.commit('deleteOne')
  },
  modules: {
    auth
  }
});